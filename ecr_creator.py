import boto3

ecr_client = boto3.client('ecr')

reponse = ecr_client.create_repository(
    repositoryName = "system-monitor-app-repository"
)

repository_uri = reponse['repository']['repositoryUri']
print(repository_uri)